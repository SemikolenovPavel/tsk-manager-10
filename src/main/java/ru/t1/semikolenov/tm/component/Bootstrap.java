package ru.t1.semikolenov.tm.component;

import ru.t1.semikolenov.tm.api.controller.ICommandController;
import ru.t1.semikolenov.tm.api.controller.IProjectController;
import ru.t1.semikolenov.tm.api.controller.ITaskController;
import ru.t1.semikolenov.tm.api.repository.ICommandRepository;
import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.api.repository.ITaskRepository;
import ru.t1.semikolenov.tm.api.service.ICommandService;
import ru.t1.semikolenov.tm.api.service.IProjectService;
import ru.t1.semikolenov.tm.api.service.ITaskService;
import ru.t1.semikolenov.tm.constant.ArgumentConst;
import ru.t1.semikolenov.tm.constant.TerminalConst;
import ru.t1.semikolenov.tm.controller.CommandController;
import ru.t1.semikolenov.tm.controller.ProjectController;
import ru.t1.semikolenov.tm.controller.TaskController;
import ru.t1.semikolenov.tm.repository.CommandRepository;
import ru.t1.semikolenov.tm.repository.ProjectRepository;
import ru.t1.semikolenov.tm.repository.TaskRepository;
import ru.t1.semikolenov.tm.service.CommandService;
import ru.t1.semikolenov.tm.service.ProjectService;
import ru.t1.semikolenov.tm.service.TaskService;
import ru.t1.semikolenov.tm.util.TerminalUtil;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        if (processArgument(args))
            System.exit(0);
        commandController.showWelcome();
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void close() {
        System.exit(0);
    }

}
