package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    void clear();

}
