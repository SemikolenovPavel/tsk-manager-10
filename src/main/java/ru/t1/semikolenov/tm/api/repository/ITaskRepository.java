package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void remove(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    void clear();

}
